import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] numbers = {205, 3506, 208, 25068, 50};
        NumbersAndStringProcessor numbersProcessor = new NumbersAndStringProcessor(numbers);

        //развернуть исходное число
        NumbersProcess numbersProcessOne = number -> {
            String s = "";
            int processedNumber;
            while (number != 0){
                processedNumber = number % 10;
                number /= 10;
                s += processedNumber;
            }
            return Integer.parseInt(s);
        };

        //убрать нули из исходного числа
        NumbersProcess numbersProcessTwo = number -> {
            String s = "";
            int processedNumber;
            while (number != 0){
                processedNumber = number % 10;
                if (processedNumber != 0){
                    s = processedNumber + s;
                }
                number /= 10;
            }
            return Integer.parseInt(s);
        };

        //заменить нечетные цифры ближайшей четной снизу (например, 7 на 6)
        NumbersProcess numbersProcessThree = number -> {
            String s = "";
            int processedNumber;
            while (number != 0){
                processedNumber = number % 10;
                if (processedNumber % 2 != 0){
                    processedNumber -= 1;
                }
                number /= 10;
                s = processedNumber + s;
            }
            return Integer.parseInt(s);
        };

        System.out.println("Исходный массив чисел: " + Arrays.toString(numbers));
        System.out.println("Массив развернутых чисел: " + Arrays.toString(numbersProcessor.process(numbersProcessOne)));
        System.out.println("Убрали нули в исходных числах: " + Arrays.toString(numbersProcessor.process(numbersProcessTwo)));
        System.out.println("Заменили нечетные цифры ближайшей четной снизу: " + Arrays.toString(numbersProcessor.process(numbersProcessThree)));
        System.out.println("______________________________________________________________________________");

        String[] strings = {"Один 1", "Два 2", "Три 3", "Четыре 4", "Пять 5"};
        NumbersAndStringProcessor stringsProcessor = new NumbersAndStringProcessor(strings);

        //развернуть исходную строку
        StringsProcess stringsProcessOne = process -> {
            char[] chars = process.toCharArray();
            String string = "";
            for (int i = chars.length-1; i >= 0; i--){
                string += chars[i];
            }
            return string;
        };

        //убрать все цифры из строки
        StringsProcess stringsProcessTwo = process -> {
            char[] chars = process.toCharArray();
            char[] charsDigit = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            String string = "";
            boolean isDigit = false;
            for (int i = 0; i < chars.length; i++){
                for (int j = 0; j < charsDigit.length; j++){
                    if (chars[i] == charsDigit[j]) {
                        isDigit = true;
                        break;
                    }
                }
                if(!isDigit){
                    string += chars[i];
                }
            }
            return string;
        };
        //сделать все маленькие буквы БОЛЬШИМИ
        StringsProcess stringsProcessThree = process -> process.toUpperCase();

        System.out.println("Исходный массив строк: " + Arrays.toString(strings));
        System.out.println("Развернули исходную строку: " + Arrays.toString(stringsProcessor.process(stringsProcessOne)));
        System.out.println("Убрали все цифры из строки: " + Arrays.toString(stringsProcessor.process(stringsProcessTwo)));
        System.out.println("Все маленькие буквы стали большими: " + Arrays.toString(stringsProcessor.process(stringsProcessThree)));
    }
}
