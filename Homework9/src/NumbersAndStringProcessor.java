public class NumbersAndStringProcessor {
        private String[] processedStrings;
        private int[] processedNumbers;
        private String[] strings;
        private int[] numbers;

        public NumbersAndStringProcessor(String[] strings){
            this.strings = strings;
            this.processedStrings = new String[strings.length];

        }
        public NumbersAndStringProcessor(int[] numbers){
            this.numbers = numbers;
            this.processedNumbers = new int[numbers.length];
        }

        public String[] process(StringsProcess process){
            for(int i = 0; i < strings.length; i++){
                processedStrings[i] = process.process(strings[i]);
            }
            return processedStrings;
        }

        public int[] process(NumbersProcess process){
            for(int i = 0; i < numbers.length; i++){
                processedNumbers[i] = process.process(numbers[i]);
            }
            return processedNumbers;
        }
}
