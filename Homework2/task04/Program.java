/*Реализовать приложение, которое меняет местами максимальный и минимальный элементы
массива (массив вводится с клавиатуры - по одному в каждой новой строке). */

import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int max = array[0];
        int min = array[0];
        int positionMax = 0;
        int positionMin = 0;
        for(int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
            if(array[i] > max){
                max = array[i];
                positionMax = i;
            }
            if(array[i] < min){
                min = array[i];
                positionMin = i;
            }
        }
        int temp = array[positionMax];
        array[positionMax] = array[positionMin];
        array[positionMin] = temp;
        System.out.println(Arrays.toString(array));
    }
}
