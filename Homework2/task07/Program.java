/*Реализовать приложение, которое заполняет двумерный массив M*N последовательностью
чисел “по спирали”. */

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность двумерного массива МxN");
        System.out.print("m = ");
        int m = scanner.nextInt();
        System.out.print("n = ");
        int n = scanner.nextInt();
        int[][] array = new int[m][n];
        int count = 0; // количество элементов
        // вправо
        for (int j = 0; j < n; j++) {
            count++;
            array[0][j] = count;

        }
        //вниз
        for (int i = 1; i < m; i++) {
            count++;
            array[i][n-1] = count;

        }
        //влево
        for (int j = n - 2; j >= 0; j--) {
            count++;
            array[m-1][j] = count;

        }
        //вверх
        for (int i = m - 2; i >= 1; i--) {
            count++;
            array[i][0] = count;

        }
        int i = 1;
        int j = 1;
        while (count < m*n-1){
            //вправо
            while (array[i][j+1] == 0){
                count++;
                array[i][j] = count;
                j++;
            }
            //вниз
            while (array[i+1][j] == 0){
                count++;
                array[i][j] = count;
                i++;
            }
            //влево
            while (array[i][j - 1] == 0){
                count++;
                array[i][j] = count;
                j--;
            }
            //ввех
            while (array[i-1][j] == 0){
                count++;
                array[i][j] = count;
                i--;
            }
        }
        for (int x = 0; x < m; x++){
            for (int y = 0; y < n; y++){
                if(array[x][y] == 0){
                    count++;
                    array[x][y] = count;

                }
            }
        }
        for (int x = 0; x < m; x++){
            for (int y = 0; y < n; y++){
                System.out.print(array[x][y] + " ");
            }
            System.out.println();
        }
    }
}
