/*Реализовать приложение, которое выполняет сортировку массива методом пузырька.*/

import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        int[] array = {5, 4, -10, 2, 0, -5, 28};
        System.out.println(Arrays.toString(array));
        for (int j = array.length - 1; j >= 1; j--){
            for (int i = 0; i < j; i++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
