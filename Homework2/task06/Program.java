/*Реализовать приложение, которое выполняет преобразование массива в число. */

class Program {
    public static void main(String args[]) {
        int[] array = {4, 2, 3, 5, 7};
        int number = 0;
        int k = 1;
        for (int i = 0; i < array.length; i++) {
            int arrayElement = array[i];
            int quantityNumber = array.length;
            while (quantityNumber-k > 0) {
                arrayElement = arrayElement * 10;
                quantityNumber--;
            }
            number = number + arrayElement;
            k++;
        }
        System.out.println(number); // программа должна вывести 42357
    }
}
