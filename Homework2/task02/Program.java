/*Реализовать приложение, которое выполняет разворот массива (массив вводится с
клавиатуры - по одному числу в каждой новой строке).*/

import java.util.Arrays;
import java.util.Scanner;

class Program {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите рамерность массива: ");
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Введите элементы массива:");
        for(int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println("Исходный массив: " + Arrays.toString(array));
        int j = 0;
        for (int i = array.length - 1; i >= array.length/2; i--){
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            j++;
        }
        System.out.println("Развернутый массив: " + Arrays.toString(array));
    }
}
