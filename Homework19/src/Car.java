import java.util.Objects;
import java.util.StringJoiner;

/**
 * Homework19
 * 01.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class Car {
    private String number;
    private String model;
    private String color;
    private Long mileage;
    private Long cost;

    public Car(String number, String model, String color, Long mileage, Long cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.cost = cost;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Long getMileage() {
        return mileage;
    }

    public Long getCost() {
        return cost;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(number, car.number) && Objects.equals(model, car.model) && Objects.equals(color, car.color) && Objects.equals(mileage, car.mileage) && Objects.equals(cost, car.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, mileage, cost);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("number='" + number + "'")
                .add("model='" + model + "'")
                .add("color='" + color + "'")
                .add("mileage=" + mileage)
                .add("cost=" + cost)
                .toString();
    }
}
