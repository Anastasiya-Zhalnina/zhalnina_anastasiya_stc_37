import java.util.List;

/**
 * Homework19
 * 01.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface CarsRepository {
    /**
     * Поиск всех автомобилей
     *
     * @return список всех автомобилей
     */
    List<Car> findAll();

    /**
     * Поиск автомобилей по цвету и пробегу
     *
     * @param color   - цвет автомобиля
     * @param mileage - пробег автомобиля
     * @return список номеров найденных автомобилей
     */
    List<String> findAllByColorOrMileage(String color, Long mileage);

    /**
     * Поиск уникальных автомобилей в заданном ценновом диапозоне
     *
     * @param costStart - нижняя граница цены
     * @param costEnd   - верхняя граница цены
     * @return количество уникальных автомобилей в заданном диапозоне
     */
    Long findUniqueModels(Long costStart, Long costEnd);

    /**
     * Поиск автомобиля с минимальной стоимостью
     */
    void findAllByMinCoast();

    /**
     * Расчитывает  среднюю стоимость автомобиля заднной модели
     *
     * @param model - модель автомобиля
     */
    void AverageCost(String model);
}
