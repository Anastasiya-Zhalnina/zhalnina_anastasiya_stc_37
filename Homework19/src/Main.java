/**
 * Homework19
 * 01.04.2021
 * <p>
 * Подготовить файл с записями, имеющими следующую структуру:
 * [НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
 * Используя Java Stream API, вывести:
 * 1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
 * 2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
 * 3) Вывести цвет автомобиля с минимальной стоимостью.
 * 4) Среднюю стоимость Camry.
 *
 * @author Anastasiya Zhalnina
 */
public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFilesImpl("cars_db.txt");
        for (Car element : carsRepository.findAll()) {
            System.out.println(element);
        }

        for (String element : carsRepository.findAllByColorOrMileage("Black", 0L)) {
            System.out.println(element);
        }

        carsRepository.findUniqueModels(700000L, 800000L);

        carsRepository.findAllByMinCoast();

        carsRepository.AverageCost("Toyota Camry");
    }
}
