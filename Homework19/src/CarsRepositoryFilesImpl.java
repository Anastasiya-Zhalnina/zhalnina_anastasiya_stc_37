import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Homework19
 * 01.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class CarsRepositoryFilesImpl implements CarsRepository {

    private String fileName;

    private static final Function<String, Car> carMapper = line -> {
        String[] parsedLine = line.split("#");
        return new Car(parsedLine[0],
                parsedLine[1],
                parsedLine[2],
                Long.parseLong(parsedLine[3]),
                Long.parseLong(parsedLine[4]));
    };

    public CarsRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> findAll() {
        List<Car> car = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("Список всех автомобилей:");
            return reader
                    .lines()
                    .map(carMapper)
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка, файл не найден!");
        } catch (IOException e) {
            System.err.println("Ошибка ввода/вывода!");
        }
        return car;
    }

    @Override
    public List<String> findAllByColorOrMileage(String color, Long mileage) {
        List<String> number = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("**********************************************************************************************************");
            System.out.println("Номера всех автомобилей, имеющих заданный цвет или заданный пробег:");
            return reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage))
                    .map(Car::getNumber)
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка, файл не найден!");
        } catch (IOException e) {
            System.err.println("Ошибка ввода/вывода!");
        }
        return number;
    }

    @Override
    public Long findUniqueModels(Long costStart, Long coastEnd) {
        Long countCar = 0L;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("**********************************************************************************************************");
            System.out.print("Количество уникальных моделей в заданном ценовом диапазоне: ");
            countCar = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getCost() >= costStart && car.getCost() <= coastEnd)
                    .distinct()
                    .count();
            System.out.println(countCar + " модели");
            return countCar;
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка, файл не найден!");
        } catch (IOException e) {
            System.err.println("Ошибка ввода/вывода!");
        }
        return countCar;
    }

    @Override
    public void findAllByMinCoast() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("**********************************************************************************************************");
            System.out.print("Цвет автомобиля с минимальной стоимостью: ");
            Optional<Car> car = reader
                    .lines()
                    .map(carMapper)
                    .min(Comparator.comparingLong(Car::getCost));
            if (car.isPresent()) {
                System.out.println(car.get().getColor());
            } else {
                System.err.println("Автомобиль с минимальной стоимостью отсутствует!");
            }
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка, файл не найден!");
        } catch (IOException e) {
            System.err.println("Ошибка ввода/вывода!");
        }
    }

    @Override
    public void AverageCost(String model) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("**********************************************************************************************************");
            System.out.print("Средняя стоимость заданной модели: ");
            List<Long> cost = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getModel().equals(model))
                    .map(Car::getCost)
                    .collect(Collectors.toList());
            OptionalDouble average = cost.stream().mapToDouble(e -> e).average();
            if (average.isPresent()) {
                System.out.printf("%.2f", average.getAsDouble());
            } else {
                System.err.println("Такой модели в списке нет!");
            }
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка, файл не найден!");
        } catch (IOException e) {
            System.err.println("Ошибка ввода/вывода!");
        }
    }
}


