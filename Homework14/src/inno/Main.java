package inno;

import inno.hashmap.HashMapImpl;

public class Main {
    public static void main(String[] args) {
	    Map<String, String> map = new HashMapImpl<>();

	    map.put("Марсель", "Сидиков");
	    map.put("Виктор", "Евлампьев");
	    map.put("Айрат", "Мухутдинов");
	    map.put("Даниил", "Вдовинов");
	    map.put("Даниил", "Богомолов");
	    map.put("Джамиль", "Садыков");
	    map.put("Николай", "Пономарев");
	    map.put("Siblings", "HELLO1");
	    map.put("Teheran", "HELLO2");


		System.out.println(map.get("Марсель")); //Сидиков
		System.out.println(map.get("Джамиль")); //Садыков
		System.out.println(map.get("Николай")); //Пономарев
		System.out.println(map.get("Иванов")); //null
		System.out.println(map.get("Siblings")); //HELLO1
		System.out.println(map.get("Даниил")); //Богомолов
		System.out.println(map.get("Виктор")); //Евлампьев

    }
}
