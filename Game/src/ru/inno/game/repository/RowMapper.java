package ru.inno.game.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Game
 * 12.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface RowMapper<T> {
    T mapRow(ResultSet row) throws SQLException;
}

