package ru.inno.game.repository;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

import static ru.inno.game.utils.JdbcUtil.closeJdbcObjects;

/**
 * Game
 * 12.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class PlayersRepositoryJdbcImpl implements PlayersRepository {
    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NICKNAME =
            "select player.name as player_name, * " +
            "from player " +
            "where name = ?";

    //language=SQL
    private static final String SQL_INSERT_PLAYER =
            "insert into player(ipaddress, name, points, countwins, countloses) " +
            "values (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_ID =
            "update player " +
            "set ipaddress  = ?," +
            "    name       = ?," +
            "    points     = ?," +
            "    countwins  = ?," +
            "    countloses = ? " +
            "where id = ?";

    static private final RowMapper<Player> playerRowMapper = row -> new Player(
            row.getLong("id"),
            row.getString("ipAddress"),
            row.getString("name"),
            row.getInt("points"),
            row.getInt("countWins"),
            row.getInt("countLoses")
    );

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByNickname(String nickname) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Player player = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME);
            statement.setString(1, nickname);
            rows = statement.executeQuery();
            if (rows.next()) {
                player = playerRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return player;
    }

    @Override
    public void save(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, player.getIpAddress());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getCountWins());
            statement.setInt(5, player.getCountLoses());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                player.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void update(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_ID);
            statement.setString(1, player.getIpAddress());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getCountWins());
            statement.setInt(5, player.getCountLoses());
            statement.setLong(6, player.getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }
}
