package ru.inno.game.repository;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

import static ru.inno.game.utils.JdbcUtil.closeJdbcObjects;

/**
 * Game
 * 13.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class ShotsRepositoryJdbcImpl implements ShotsRepository {
    //language=SQL
    private static final String SQL_INSERT_SHOT =
            "insert into shot(shottime, game_id, shooter_id, target_id) " +
            "values (?, ?, ?, ?)";

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS);
            statement.setObject(1, shot.getShotTime());
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4, shot.getTarget().getId());
            // сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()) {
                // получаем ключ, который сгенерировала база ДЛЯ ТЕКУЩЕГО ЗАПРОСА
                shot.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }
}
