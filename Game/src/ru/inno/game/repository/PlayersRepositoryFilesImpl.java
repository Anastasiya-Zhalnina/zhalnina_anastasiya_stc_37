package ru.inno.game.repository;

import ru.inno.game.models.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Game
 * 30.03.2021
 *
 * @author Anastasiya Zhalnina
 */
public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private String fileName;
    private String sequencePlayerFileName;
    private Map<String, Player> players;

    public PlayersRepositoryFilesImpl(String fileName, String sequencePlayerFileName) {
        this.fileName = fileName;
        this.sequencePlayerFileName = sequencePlayerFileName;
        players = new HashMap<>();
    }

    @Override
    public Player findByNickname(String nickname) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String string;
            while ((string = reader.readLine()) != null ) {
                if (!string.equals("")) {
                    String[] name = string.split("#");
                    if (nickname.equals(name[2])) {
                        return players.get(nickname);
                    }
                }
            }
            reader.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            player.setId(generatePlayerId());
            writer.write(player.getId() + "#" + player.getIpAddress() + "#" + player.getName() + "#"
                    + player.getPoints() + "#" +  player.getCountWins() + "#" + player.getCountLoses() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        players.put(player.getName(), player);
    }

    private Long generatePlayerId(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequencePlayerFileName));
            String lastGeneratedIdAsString = reader.readLine();
            Long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequencePlayerFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();
            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        List<String> listPlayer = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String string;
            while ((string = reader.readLine()) != null){
                listPlayer.add(string + "\n");
            }
            listPlayer.removeIf(players -> players.equals("" + "\n"));
            reader.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        String s = player.getId() + "#" + player.getIpAddress() + "#" + player.getName() + "#"
                + player.getPoints() + "#" +  player.getCountWins() + "#" + player.getCountLoses() + "\n";
        for (String element: listPlayer){
            String[] argumentsPlayer = element.split("#");
            if(Long.parseLong(argumentsPlayer[0]) == player.getId()){
                listPlayer.set(listPlayer.indexOf(element), s);
            }
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                for(String elements: listPlayer){
                    writer.write(elements);
                }
                writer.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}

