package ru.inno.game.repository;

import ru.inno.game.models.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 */
public class PlayersRepositoryMapImpl implements PlayersRepository {

    private Map<String, Player> players;

    public PlayersRepositoryMapImpl() {
        players = new HashMap<>();
    }

    @Override
    public Player findByNickname(String nickname) {
        return players.get(nickname);
    }

    @Override
    public void save(Player player) {
        players.put(player.getName(), player);
    }

    @Override
    public void update(Player player) {
        if(players.containsKey(player.getName())) {
            players.put(player.getName(), player);
        }else{
            System.err.println("Информация не обновлена, т.к. игрока с данным именем нет в базе");
        }
    }
}

