package ru.inno.game.repository;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

import static ru.inno.game.utils.JdbcUtil.closeJdbcObjects;

/**
 * Game
 * 13.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class GameRepositoryJdbcImpl implements GameRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME =
            "insert into game(datetime, playerfirst_id, playersecond_id, countshotsplayerfirst, countshotsplayersecond," +
            "                 secondsgametimeamount) " +
            "values (?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_FIND_GAME_BY_ID =
                    "select g.id                     as game_id," +
                    "       g.dateTime               as game_datetime," +
                    "       p.id                     as playerfirst_id," +
                    "       p.ipAddress              as playerfirst_ip," +
                    "       p.name                   as playerfirst_name," +
                    "       p.points                 as playerfirst_points," +
                    "       p.countwins              as playerfirst_countwins," +
                    "       p.countLoses             as playerfirst_countloses," +
                    "       p2.id                    as playersecond_id," +
                    "       p2.ipAddress             as playersecond_ip," +
                    "       p2.name                  as playersecond_name," +
                    "       p2.points                as playersecond_points," +
                    "       p2.countwins             as playersecond_countwins," +
                    "       p2.countLoses            as playersecond_countloses," +
                    "       g.countShotsPlayerFirst  as playerfirst_countshots," +
                    "       g.countShotsPlayerSecond as playersecond_countshots," +
                    "       g.secondsGameTimeAmount  as game_secondsgame " +
                    "from game as g" +
                    "         join player as p on g.playerFirst_id = p.id" +
                    "         join player p2 on g.playerSecond_id = p2.id " +
                    "where g.id = ?";

    //language=SQl
    private static final String SQL_UPDATE_GAME_BY_ID =
            "update game " +
            "set datetime               = ?," +
            "    playerfirst_id         = ?," +
            "    playersecond_id        = ?," +
            "    countshotsplayerfirst  = ?," +
            "    countshotsplayersecond = ?," +
            "    secondsgametimeamount  =?" +
            "where id = ?";

    static private final RowMapper<Player> playerFirstRowMapper = row -> new Player(
            row.getLong("playerfirst_id"),
            row.getString("playerfirst_ip"),
            row.getString("playerfirst_name"),
            row.getInt("playerfirst_points"),
            row.getInt("playerfirst_countwins"),
            row.getInt("playerfirst_countloses")
    );

    static private final RowMapper<Player> playerSecondRowMapper = row -> new Player(
            row.getLong("playersecond_id"),
            row.getString("playersecond_ip"),
            row.getString("playersecond_name"),
            row.getInt("playersecond_points"),
            row.getInt("playersecond_countwins"),
            row.getInt("playersecond_countloses")
    );

    static private final RowMapper<Game> gameRowMapper = row -> new Game(
            row.getLong("game_id"),
            row.getTimestamp("game_datetime").toLocalDateTime(),
            playerFirstRowMapper.mapRow(row),
            playerSecondRowMapper.mapRow(row),
            row.getInt("playerfirst_countshots"),
            row.getInt("playersecond_countshots"),
            row.getLong("game_secondsgame")
    );

    private DataSource dataSource;

    public GameRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS);
            statement.setObject(1, game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getCountShotsPlayerFirst());
            statement.setInt(5, game.getCountShotsPlayerSecond());
            statement.setLong(6, game.getSecondsGameTimeAmount());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                game.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public Game findById(Long gameId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Game game = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID);
            statement.setLong(1, gameId);
            rows = statement.executeQuery();

            if (rows.next()) {
                game = gameRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return game;
    }

    @Override
    public void update(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID);
            statement.setObject(1, game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getCountShotsPlayerFirst());
            statement.setInt(5, game.getCountShotsPlayerSecond());
            statement.setLong(6, game.getSecondsGameTimeAmount());
            statement.setLong(7, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }
}
