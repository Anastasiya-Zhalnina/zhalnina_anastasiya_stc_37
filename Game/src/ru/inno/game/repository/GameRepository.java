package ru.inno.game.repository;

import ru.inno.game.models.Game;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface GameRepository {
    /**
     * Метод сохранения игры
     * @param game - игра
     */
    void save(Game game);

    /**
     * Метод поиска игры по идентификатору
     * @param gameId - идентификатор игры
     * @return - игра по заданному идентификатору
     */
    Game findById(Long gameId);

    /**
     * Метод обновления игры
     * @param game - игра
     */
    void update(Game game);
}

