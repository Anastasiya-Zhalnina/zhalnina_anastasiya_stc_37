package ru.inno.game.repository;

import ru.inno.game.models.Shot;

/**
 * Game
 * 26.03.2021
 *
 * @author Anastasiya Zhalnina
 */

public interface ShotsRepository {
    /**
     * Метод, сохранения выстрела
     * @param shot - выстрел
     */
    void save(Shot shot);
}
