package ru.inno.game.repository;

import ru.inno.game.models.Shot;

import java.io.*;

/**
 * Game
 * 30.03.2021
 *
 * @author Anastasiya Zhalnina
 *
 * Имплементация на основе файла
 */
public class ShotsRepositoryFilesImpl implements ShotsRepository {
    private String dbFileName;
    private String sequenceFileName;

    public ShotsRepositoryFilesImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Shot shot) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true));
            shot.setId(generateId());
            writer.write(shot.getId() + "#" + shot.getShotTime().toString() + "#" + shot.getGame().getId() + "#" +
                    shot.getShooter().getName() + "#" + shot.getTarget().getName() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Long generateId(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastGeneratedIdAsString = reader.readLine();
            Long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();
            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}

