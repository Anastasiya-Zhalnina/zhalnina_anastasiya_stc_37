package ru.inno.game.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 *
 * Класс - игра.
 */
public class Game {
    private Long id;                                                    //Идентификатор игры
    private LocalDateTime dateTime;                                     //Дата
    private Player playerFirst, playerSecond;                           //Игроки
    private Integer countShotsPlayerFirst, countShotsPlayerSecond;      //Количество выстрелов от каждого игрока
    private Long secondsGameTimeAmount;                                 //Длительность игры

    public Game(LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer countShotsPlayerFirst, Integer countShotsPlayerSecond, Long secondsGameTimeAmount) {
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.countShotsPlayerFirst = countShotsPlayerFirst;
        this.countShotsPlayerSecond = countShotsPlayerSecond;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public Game(Long id, LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer countShotsPlayerFirst, Integer countShotsPlayerSecond, Long secondsGameTimeAmount) {
        this.id = id;
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.countShotsPlayerFirst = countShotsPlayerFirst;
        this.countShotsPlayerSecond = countShotsPlayerSecond;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Player getPlayerFirst() {
        return playerFirst;
    }

    public Player getPlayerSecond() {
        return playerSecond;
    }

    public Integer getCountShotsPlayerFirst() {
        return countShotsPlayerFirst;
    }

    public Integer getCountShotsPlayerSecond() {
        return countShotsPlayerSecond;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    public Long getId() {
        return id;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setPlayerFirst(Player playerFirst) {
        this.playerFirst = playerFirst;
    }

    public void setPlayerSecond(Player playerSecond) {
        this.playerSecond = playerSecond;
    }

    public void setCountShotsPlayerFirst(Integer countShotsPlayerFirst) {
        this.countShotsPlayerFirst = countShotsPlayerFirst;
    }

    public void setCountShotsPlayerSecond(Integer countShotsPlayerSecond) {
        this.countShotsPlayerSecond = countShotsPlayerSecond;
    }

    public void setSecondsGameTimeAmount(Long secondsGameTimeAmount) {
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(id, game.id) && Objects.equals(dateTime, game.dateTime) && Objects.equals(playerFirst, game.playerFirst) && Objects.equals(playerSecond, game.playerSecond) && Objects.equals(countShotsPlayerFirst, game.countShotsPlayerFirst) && Objects.equals(countShotsPlayerSecond, game.countShotsPlayerSecond) && Objects.equals(secondsGameTimeAmount, game.secondsGameTimeAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, playerFirst, playerSecond, countShotsPlayerFirst, countShotsPlayerSecond, secondsGameTimeAmount);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Game.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("dateTime=" + dateTime)
                .add("playerFirst=" + playerFirst)
                .add("playerSecond=" + playerSecond)
                .add("countShotsPlayerFirst=" + countShotsPlayerFirst)
                .add("countShotsPlayerSecond=" + countShotsPlayerSecond)
                .add("secondsGameTimeAmount=" + secondsGameTimeAmount)
                .toString();
    }
}
