package ru.inno.game.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 *
 * Класс - выстрел.
 */
public class Shot {
    private Long id;                    //Идентификатор игры
    private LocalDateTime shotTime;     //Время выстрела
    private Game game;                  //В какой игре
    private Player shooter;             //Стрелок (кто попал)
    private Player target;              //Мишень (в кого попали)

    public Shot(LocalDateTime shotTime, Game game, Player shooter, Player target) {
        this.shotTime = shotTime;
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public Shot(Long id, LocalDateTime shotTime, Game game, Player shooter, Player target) {
        this.id = id;
        this.shotTime = shotTime;
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public LocalDateTime getShotTime() {
        return shotTime;
    }

    public Game getGame() {
        return game;
    }

    public Player getShooter() {
        return shooter;
    }

    public Player getTarget() {
        return target;
    }

    public Long getId() {
        return id;
    }

    public void setShotTime(LocalDateTime shotTime) {
        this.shotTime = shotTime;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setShooter(Player shooter) {
        this.shooter = shooter;
    }

    public void setTarget(Player target) {
        this.target = target;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(id, shot.id) && Objects.equals(shotTime, shot.shotTime) && Objects.equals(game, shot.game) && Objects.equals(shooter, shot.shooter) && Objects.equals(target, shot.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, shotTime, game, shooter, target);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Shot.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("shotTime=" + shotTime)
                .add("game=" + game)
                .add("shooter=" + shooter)
                .add("target=" + target)
                .toString();
    }
}
