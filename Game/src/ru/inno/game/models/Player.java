package ru.inno.game.models;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 *
 * Класс - пользователь.
 */
public class Player {
    private Long id;                //Идентификатор игры
    private String ipAddress;       //IP-адрес, с которого он заходил в последний раз
    private String name;            //Имя пользователя
    private Integer points;         //Количество очков
    private Integer countWins;      //Количество побед
    private Integer countLoses;     //Количество поражений


    public Player(String ipAddress, String name, Integer maxPoints, Integer countWins, Integer countLoses) {
        this.ipAddress = ipAddress;
        this.name = name;
        this.points = maxPoints;
        this.countWins = countWins;
        this.countLoses = countLoses;
    }

    public Player(Long id, String ipAddress, String name, Integer points, Integer countWins, Integer countLoses) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.name = name;
        this.points = points;
        this.countWins = countWins;
        this.countLoses = countLoses;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getName() {
        return name;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getCountWins() {
        return countWins;
    }

    public Integer getCountLoses() {
        return countLoses;
    }

    public Long getId() {
        return id;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPoints(Integer maxPoints) {
        this.points = maxPoints;
    }

    public void setCountWins(Integer countWins) {
        this.countWins = countWins;
    }

    public void setCountLoses(Integer countLoses) {
        this.countLoses = countLoses;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(ipAddress, player.ipAddress) && Objects.equals(name, player.name) && Objects.equals(points, player.points) && Objects.equals(countWins, player.countWins) && Objects.equals(countLoses, player.countLoses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ipAddress, name, points, countWins, countLoses);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Player.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("ipAddress='" + ipAddress + "'")
                .add("name='" + name + "'")
                .add("points=" + points)
                .add("countWins=" + countWins)
                .add("countLoses=" + countLoses)
                .toString();
    }
}
