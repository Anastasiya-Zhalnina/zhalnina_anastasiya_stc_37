package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;


/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 */

public interface GameService {
    /**
     * Метод вызывается для обеспечения начала игры.
     * Если игрок с таким именем есть, то мы работаем с этим игроком, если нет - создаем нового игрока.
     * @param firstIp - IP-адресс с которого зашел первый игрок.
     * @param secondIp - IP-адресс с которого зашел второй игрок.
     * @param firstPlayerNickname - имя первого игрока.
     * @param secondPlayerNickname - имя второго игрока.
     * @return идентификатор игры.
     */
    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    /**
     * Метод фиксирует выстрел игроков (попавшие)
     * @param gameId - идентификатор игры в рамках которой был произведен выстрел
     * @param shooterNickname - имя игрока который стрелял
     * @param targetNickname - имя игрока, в  которого стреляли
     */
    void shot(Long gameId, String shooterNickname, String targetNickname);

    /**
     * Метод реализующий конец игры
     * @param gameId - идентификатор игры
     * @return
     */
    StatisticDto finishGame(Long gameId);
}
