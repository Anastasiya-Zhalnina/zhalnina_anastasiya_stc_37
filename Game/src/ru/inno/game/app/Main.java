package ru.inno.game.app;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repository.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;
import ru.inno.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/Game";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "e47Y6xLbdA";

    public static void main(String[] args) {

//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
//        GameRepository gameRepository = new GameRepositoryListImpl();
//        ShotsRepository shotsRepository = new ShotsRepositoryListImpl();
//        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl("players_db.txt", "players_sequence.txt");
//        ShotsRepository shotsRepository = new ShotsRepositoryFilesImpl("shots_db.txt", "shots_sequence.txt");

        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GameRepository gameRepository = new GameRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gameRepository, shotsRepository);
        int countGames = 0;
        while (countGames < 3) {
            System.out.println("-------------Game-------------");
            Scanner scanner = new Scanner(System.in);
            System.out.print("Первый игрок введите своё имя: ");
            String fistPlayerName = scanner.nextLine();
            System.out.print("Второй игрок введите своё имя: ");
            String secondPlayerName = scanner.nextLine();
            Random random = new Random();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", fistPlayerName, secondPlayerName);
            String shooter = fistPlayerName;
            String target = secondPlayerName;
            int countShots = 0;
            while (countShots < 10) {
                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();
                int success = random.nextInt(2);
                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }
                String temp = shooter;
                shooter = target;
                target = temp;
                countShots++;
            }
            System.out.println("__________________________________________________________");
            StatisticDto statisticDto = gameService.finishGame(gameId);
            System.out.println(statisticDto);
            countGames++;
            System.out.println();
        }
    }
}
