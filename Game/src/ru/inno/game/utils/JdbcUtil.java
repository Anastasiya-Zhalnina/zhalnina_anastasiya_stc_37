package ru.inno.game.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Game
 * 12.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class JdbcUtil {

    public static void closeJdbcObjects(Connection connection, Statement statement, ResultSet rows) {
        if (rows != null) {
            try {
                rows.close();
            } catch (SQLException ignore) {
            }
        }

        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
            }
        }

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ignore) {
            }
        }
    }
}
