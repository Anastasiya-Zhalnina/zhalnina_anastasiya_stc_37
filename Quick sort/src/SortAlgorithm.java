/**
 * Quick sort
 * 06.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface SortAlgorithm {
    /**
     * Сортировка массива
     * @param array - сортируемый массив
     */
    void sort(int[] array);
}
