import java.util.Arrays;

/**
 * Quick sort
 * 06.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class Main {
    public static void main(String[] args) {
       SortAlgorithm sortAlgorithm = new QuickSort();
       Sorter sorter = new Sorter();
       sorter.setSortAlgorithm(sortAlgorithm);
       int[] array = {9, -3, 5, 2, 6, 8, -6, 1, 3};
       sorter.sort(array);
       System.out.println(Arrays.toString(array));

    }
}
