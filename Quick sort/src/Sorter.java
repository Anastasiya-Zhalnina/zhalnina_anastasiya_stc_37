/**
 * Quick sort
 * 06.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class Sorter {
    private SortAlgorithm sortAlgorithm;

    public void setSortAlgorithm(SortAlgorithm sortAlgorithm) {
        this.sortAlgorithm = sortAlgorithm;
    }

    public void sort(int[] array) {
        sortAlgorithm.sort(array);
    }
}
