public abstract class Ellipse extends GeometricShape{
    public Ellipse(double a, double b, double centerX, double centerY) {
        super(a, b, centerX, centerY);
    }
}
