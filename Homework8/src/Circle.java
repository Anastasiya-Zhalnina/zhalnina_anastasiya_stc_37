public class Circle extends Ellipse {
    public Circle(double a, double b, double centerX, double centerY) {
        super(a, b, centerX, centerY);
    }

    @Override
    public void getAreaGeometricShape() {
        if (a == b){
            System.out.printf("Площадь круга S = %.2f\n", Math.PI * a * b);
        }else{
            System.out.printf("Площадь эллипса S = %.2f\n", Math.PI * a * b);
        }
        return ;
    }

    @Override
    public void getPerimeterGeometricShape() {
        if (a == b){
            System.out.printf("Периметр круга P = %.2f\n", 4 * (Math.PI * a * b + (a - b)*(a - b))/(a + b));
        }else{
            System.out.printf("Периметр эллипса P = %.2f\n", 4 * (Math.PI * a * b + (a - b)*(a - b))/(a + b));
        }
    }

    @Override
    public void scalingGeometricShape(int k) {
        super.scalingGeometricShape(k);
        if (a == b){
            System.out.println("Произошло масштабирование круга в " + k + " раз(а). Новые параметры круга: a = " + this.a + ", b = " + this.b);
        }else{
            System.out.println("Произошло масштабирование эллипса в " + k + " раз(а). Новые параметры эллипса: a = " + this.a + ", b = " + this.b);
        }
    }

    @Override
    public void movingGeometricShape(double kX, double kY) {
        super.movingGeometricShape(kX, kY);
        if (a == b){
            System.out.println("Круг был перемещен. Новые координаты центра: (" + this.centerX + "; " + this.centerY + ")");
        }else{
            System.out.println("Эллипс был перемещен. Новые координаты центра: (" + this.centerX + "; " + this.centerY + ")");
        }
    }
}
