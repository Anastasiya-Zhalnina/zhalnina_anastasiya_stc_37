public interface Relocatable {
    void movingGeometricShape(double kX, double kY);
}
