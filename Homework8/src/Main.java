public class Main {
    public static void main(String[] args) {
       Circle circle = new Circle(5,5, 3, 0);
       Circle ellipse = new Circle(6,4, 0, 2);
       Square square = new Square(3, 3,1,1);
       Square rectangle = new Square(4,3, -1,-1);
       GeometricShape[] geometricShape = {circle, ellipse, square, rectangle};
       for (GeometricShape shape : geometricShape) {
            shape.getAreaGeometricShape();
            shape.getPerimeterGeometricShape();
            shape.scalingGeometricShape(2);
            shape.getAreaGeometricShape();
            shape.getPerimeterGeometricShape();
            shape.movingGeometricShape(5,2);
            System.out.println("___________________________________________________________________________________________________________");
       }
    }
}
