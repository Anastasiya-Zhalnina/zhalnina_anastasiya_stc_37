public class Square extends Rectangle {
    public Square(double a, double b, double centerX, double centerY) {
        super(a, b, centerX, centerY);
     }

    @Override
    public void getAreaGeometricShape() {
        if (a == b){
            System.out.printf("Площадь квадрата S = %.2f\n",  a * b);
        }else {
            System.out.printf("Площадь прямоугольника S = %.2f\n",  a * b);
        }
    }

    @Override
    public void getPerimeterGeometricShape() {
        if (a == b){
            System.out.printf("Периметр квадрата P = %.2f\n", 2 * (a + b));
        }else {
            System.out.printf("Периметр прямоугольника P = %.2f\n", 2 * (a + b));
        }
    }

    @Override
    public void scalingGeometricShape(int k) {
        super.scalingGeometricShape(k);
        if (a == b){
            System.out.println("Произошло масштабирование квадрата в " + k + " раз(а). Новые параметры квадрата: a = " + this.a + ", b = " + this.b);
        }else{
            System.out.println("Произошло масштабирование прямоугольника в " + k + " раз(а). Новые параметры прямоугольника: a = " + this.a + ", b = " + this.b);
        }
    }

    @Override
    public void movingGeometricShape(double kX, double kY) {
        super.movingGeometricShape(kX, kY);
        if (a == b){
            System.out.println("Квадрат был перемещен. Новые координаты центра: (" + this.centerX + "; " + this.centerY + ")");
        }else{
            System.out.println("Прямоугольник был перемещен. Новые координаты центра: (" + this.centerX + "; " + this.centerY + ")");
        }
    }
}
