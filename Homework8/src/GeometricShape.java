public abstract class GeometricShape implements Scalable, Relocatable{
    protected double a;
    protected double b;
    protected  double centerX;
    protected  double centerY;

    public GeometricShape(double a, double b, double centerX, double centerY) {
        if(a > 0) {
            this.a = a;
        }
        if (b > 0) {
            this.b = b;
        }
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public abstract void getAreaGeometricShape();
    public abstract void getPerimeterGeometricShape();

    @Override
    public void movingGeometricShape(double kX, double kY) {
        this.centerX += kX;
        this.centerY += kY;
    }

    @Override
    public void scalingGeometricShape(int k) {
        this.a *= k;
        this.b *= k;
    }
}
