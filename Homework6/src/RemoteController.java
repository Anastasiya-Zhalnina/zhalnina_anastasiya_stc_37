import java.util.Random;

public class RemoteController {
    private final TV tv; //Ссылка на объект TV

    public RemoteController(TV tv){
        this.tv = tv;
    }

    /* Метод обращения к определенному каналу телевизора под определенным номером,
    который позволит по номеру канала “показать” СЛУЧАЙНУЮ программу этого канала. */

    public void showProgramByChannelNumber(int numberChannel){
        Random random = new Random();
        if(numberChannel < tv.getChannels().length){
            Program[] programs = this.tv.getChannels()[numberChannel].getProgramsNames();
            System.out.println("Сейчас включен канал " + this.tv.getChannels()[numberChannel].getNameChannel() + " в эфире программа " + programs[random.nextInt(this.tv.getChannels().length)].getProgramName());
        } else {
        System.out.println("Канал под данным номер отсутствует в телевизоре");
        }
    }
}
