import java.util.Random;

public class TV {
    private final Channel[] channels; // Массив каналов

    public TV() {
        this.channels = new Channel[5];
    }

    //Метод добавления каналов
    public void addChannel(Channel[] channel) {
        for (int i = 0; i < channel.length; i++) {
            this.channels[i] = channel[i];
        }
    }

    //Метод обращения к каналу по его номеру в массиве (выводит название случайной программы канала)
    public void showChannel(int numberChannel) {
        Random random = new Random();
        if (numberChannel < this.channels.length) {
            Program[] programs = this.channels[numberChannel].getProgramsNames();
            System.out.println("Сейчас включен канал " + this.channels[numberChannel].getNameChannel() + " в эфире программа " + programs[random.nextInt(this.channels.length)].getProgramName());
        } else {
            System.out.println("Канал под данным номер отсутствует в телевизоре");
        }
    }

    public Channel[] getChannels() {
        return channels;
    }
}

