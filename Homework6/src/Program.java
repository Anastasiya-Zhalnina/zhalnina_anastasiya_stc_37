public class Program {
    private final String programName; //Название программы

    public Program(String programName){
        this.programName = programName;
    }

    public String getProgramName() {
        return programName;
    }
}
