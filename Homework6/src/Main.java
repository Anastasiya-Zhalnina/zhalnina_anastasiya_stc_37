//демонстрация работоспособности программы

public class Main {
    public static void main(String[] args) {
        Channel channelOne = new Channel("Первый");
        channelOne.addProgramToChannel(new Program("Новости"));
        channelOne.addProgramToChannel(new Program("Играй гармонь любимая!"));
        channelOne.addProgramToChannel(new Program("Ледниковый период"));
        channelOne.addProgramToChannel(new Program("Чемпионат мира по биатлону 2021. Мужчины. Масс-старт. 15 км. Трансляция из Словении. Прямая трансляция."));
        channelOne.addProgramToChannel(new Program("Точь-в-точь"));
        channelOne.showPrograms();

        Channel channelRussiaOne = new Channel("Россия 1");
        channelRussiaOne.addProgramToChannel(new Program("Устами младенца"));
        channelRussiaOne.addProgramToChannel(new Program("Сто к одному"));
        channelRussiaOne.addProgramToChannel(new Program("Танцы со звездами"));
        channelRussiaOne.addProgramToChannel(new Program("Вести недели"));
        channelRussiaOne.addProgramToChannel(new Program("Москва. Кремль. Путин."));
        channelRussiaOne.showPrograms();

        Channel channelNTV = new Channel("НТВ");
        channelNTV.addProgramToChannel(new Program("Сегодня"));
        channelNTV.addProgramToChannel(new Program("Главная дорога"));
        channelNTV.addProgramToChannel(new Program("Квартирный вопрос"));
        channelNTV.addProgramToChannel(new Program("Своя игра"));
        channelNTV.addProgramToChannel(new Program("Новые русские сенсации"));
        channelNTV.showPrograms();

        Channel channelCarousel = new Channel("Карусель");
        channelCarousel.addProgramToChannel(new Program("Дракоша Тоша"));
        channelCarousel.addProgramToChannel(new Program("Жужжалка"));
        channelCarousel.addProgramToChannel(new Program("Барбоскины"));
        channelCarousel.addProgramToChannel(new Program("Съедобное или не съедобное"));
        channelCarousel.addProgramToChannel(new Program("Буба"));
        channelCarousel.showPrograms();

        Channel channelSTS = new Channel("СТС");
        channelSTS.addProgramToChannel(new Program("Ну, погоди!"));
        channelSTS.addProgramToChannel(new Program("Ералаш"));
        channelSTS.addProgramToChannel(new Program("Охотники на троллей"));
        channelSTS.addProgramToChannel(new Program("Уральские пельмени"));
        channelSTS.addProgramToChannel(new Program("Стендап Андеграунд"));
        channelSTS.showPrograms();
        System.out.println("_________________________________________________________________________________________");

        TV tv = new TV();
        Channel[] channels = {channelOne, channelRussiaOne, channelNTV, channelCarousel, channelSTS};
        tv.addChannel(channels);
        tv.showChannel(2);
        System.out.println("_________________________________________________________________________________________");

        RemoteController remoteController = new RemoteController(tv);
        remoteController.showProgramByChannelNumber(3);
    }
}
