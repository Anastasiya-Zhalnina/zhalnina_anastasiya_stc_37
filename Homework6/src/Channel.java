import java.util.Random;

public class Channel {
    //поля - назваие канала
    private String nameChannel;

    //массив программ которые будут показаны по каналу
    private Program[] programsNames;
    private int programCount;

    //конструктор принимающий имя канала
    public Channel(String nameChannel){
        this.nameChannel = nameChannel;
        this.programsNames = new Program[5];
        this.programCount = 0;
    }

    //Метод добавления программы в канал
    public void addProgramToChannel(Program programName) {
       if (programCount < programsNames.length){
           this.programsNames[programCount] = programName;
           programCount++;
       }else {
           System.err.println("Превышен лимит по дабавлению программ!");
       }
    }

    //Метод вывода названия случайной программы (показ программы)
    public void showPrograms(){
        Random random = new Random();
        System.out.println("Программа " + programsNames[random.nextInt(programCount)].getProgramName() +  " идет по каналу " + nameChannel);
    }

    public String getNameChannel() {
        return nameChannel;
    }

    public Program[] getProgramsNames() {
        return programsNames;
    }
}
