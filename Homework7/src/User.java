public class User {
    private final String firstName;
    private final String lastName;
    private final int age;
    private final boolean isWorker;

    public static Builder builder() {
        return new User.Builder();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    private User(Builder builder){
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;;
    }

    public static class Builder{
        private String firstName;
        private String lastName;
        private  int age;
        private  boolean isWorker;

        public Builder firstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public Builder age(int age){
            this.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker){
            this.isWorker = isWorker;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }
}

