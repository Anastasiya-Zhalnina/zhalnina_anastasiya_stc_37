package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GameRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 * Бизнес-логика.
 */

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;
    private GameRepository gameRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GameRepository gameRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gameRepository = gameRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickName) {
        // получили информацию об обоих игроках
        Player firstPlayer = checkIfExists(firstIp, firstPlayerNickname);
        Player secondPlayer = checkIfExists(secondIp, secondPlayerNickName);

        //создали игру
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(firstPlayer)
                .playerSecond(secondPlayer)
                .countShotsPlayerFirst(0)
                .countShotsPlayerSecond(0)
                .secondsGameTimeAmount(0L)
                .build();
        //сохранили игру в репозиторий
        gameRepository.save(game);

        return game.getId(); //возвращаем идентификатор игры
    }

    //проверяем есть ли игрок в системе
    private Player checkIfExists(String ip, String nickname) {
        //получили информацию об игроках
        Player player = playersRepository.findByNickname(nickname);
        //если игрока нет под таким именем
        if (player == null) {
            //создаем игрока
            player = Player.builder()
                    .ipAddress(ip)
                    .name(nickname)
                    .points(0)
                    .countWins(0)
                    .countLoses(0)
                    .build();
            //сохраняем информацию об игроке в репозиторий
            playersRepository.save(player);
        } else {
            //если ирок с таким именем есть -> обновляем у него IP - адрес.
            player.setIpAddress(ip);
            //обновляем информацию об игроке
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        //получаем игрока, который стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        //получаем игрока, в которого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        //получаем игру
        Game game = gameRepository.findById(gameId);
        //создаем выстрел
        Shot shot = Shot.builder()
                .shotTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();
        //увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        //если стрелявший первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            //сохраняем информацию о выстреле в игре
            game.setCountShotsPlayerFirst(game.getCountShotsPlayerFirst() + 1);
        }
        //если стрелявший второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            //сохраняем информацию о выстреле в игре
            game.setCountShotsPlayerSecond(game.getCountShotsPlayerSecond() + 1);
        }
        //обновляем данные о стреляющем
        playersRepository.update(shooter);
        //обновляем данные по игре
        gameRepository.update(game);
        //сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        //получаем игру из репозитория
        Game game = gameRepository.findById(gameId);
        //создаем объект dto
        StatisticDto statisticDto = new StatisticDto(game);
        //если у первого игрока попаданий больше
        if (game.getCountShotsPlayerFirst() > game.getCountShotsPlayerSecond()) {
            //увеличиваем количество побед у первого игрока
            game.getPlayerFirst().setCountWins(game.getPlayerFirst().getCountWins() + 1);
            //увеличиваем количество поражений у второго игрока
            game.getPlayerSecond().setCountLoses(game.getPlayerSecond().getCountLoses() + 1);
            //если у второго игрока попаданий больше
        } else if (game.getCountShotsPlayerFirst() < game.getCountShotsPlayerSecond()) {
            //увеличиваем количество поражений у первого игрока
            game.getPlayerFirst().setCountLoses(game.getPlayerFirst().getCountLoses() + 1);
            //увеличиваем количество побед у второго игрока
            game.getPlayerSecond().setCountWins(game.getPlayerSecond().getCountWins() + 1);
        }
        //обновляем данные об игроках
        playersRepository.update(game.getPlayerFirst());
        playersRepository.update(game.getPlayerSecond());
        //устанавливаем длительность игры
        long time = Duration.between(LocalTime.now(), game.getDateTime()).getSeconds();
        game.setSecondsGameTimeAmount(Math.abs(time));
        gameRepository.update(game);
        return statisticDto;
    }
}

