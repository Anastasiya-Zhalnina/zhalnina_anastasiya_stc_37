package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 * Модель предметной области (Класс - выстрел)
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Shot {
    private Long id;                    //Идентификатор игры
    private LocalDateTime shotTime;     //Время выстрела
    private Game game;                  //В какой игре
    private Player shooter;             //Стрелок (кто попал)
    private Player target;              //Мишень (в кого попали)
}
