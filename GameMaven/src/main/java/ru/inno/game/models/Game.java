package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 * Модель предметной облости (Класс - игра)
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Game {
    private Long id;                                                    //Идентификатор игры
    private LocalDateTime dateTime;                                     //Дата
    private Player playerFirst, playerSecond;                           //Игроки
    private Integer countShotsPlayerFirst, countShotsPlayerSecond;      //Количество выстрелов от каждого игрока
    private Long secondsGameTimeAmount;                                 //Длительность игры
}
