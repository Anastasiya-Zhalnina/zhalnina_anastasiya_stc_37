package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 * Модель предметной области (Класс - пользователь)
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Player {
    private Long id;                //Идентификатор игры
    private String ipAddress;       //IP-адрес, с которого он заходил в последний раз
    private String name;            //Имя пользователя
    private Integer points;         //Количество очков
    private Integer countWins;      //Количество побед
    private Integer countLoses;     //Количество поражений
}
