package ru.inno.game.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.repository.*;
import ru.inno.game.server.GameServer;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;

import javax.sql.DataSource;

/**
 * game
 * 22.04.2021
 *
 * @author Anastasiya Zhalnina
 */

public class MainServer {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/Game";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "e47Y6xLbdA";
    private static final String JDBC_DRIVER = "org.postgresql.Driver";

    public static void main(String[] args) {
        // ConnectionPool - пул соединений с базой данных
        HikariConfig configuration = new HikariConfig();
        // указываем данные для подключение
        configuration.setJdbcUrl(JDBC_URL);
        configuration.setDriverClassName(JDBC_DRIVER);
        configuration.setUsername(JDBC_USER);
        configuration.setPassword(JDBC_PASSWORD);
        configuration.setMaximumPoolSize(20);
        // создали DataSource - источник данных
        DataSource dataSource = new HikariDataSource(configuration);

        // создаем репозиторий, который использует этот источник данных
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GameRepository gameRepository = new GameRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);

        // создали сервис, который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository, gameRepository, shotsRepository);
        // передали сервис объекту-серверу для нашей игры
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
