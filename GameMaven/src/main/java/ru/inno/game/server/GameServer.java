package ru.inno.game.server;

import ru.inno.game.services.GameService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.inno.game.server.CommandsParser.*;

/**
 * game
 * 22.04.2021
 *
 * @author Anastasiya Zhalnina
 * Cервер (класс) - отвечает за подключение игроков по протоколу socket
 */

public class GameServer {
    private ClientThread firstPlayer;       // отдельный поток (Thread) для первого игрока (параллельный main)
    private ClientThread secondPlayer;      // отдельный поток (Thread) для второго игрока (параллельный main)
    private ServerSocket serverSocket;      // объект для сокет-сервера
    private boolean isGameStarted = false;  // флаг, определяет, началась игра или не началась?
    private boolean isGameInProcess = true; // игра в процессе
    private long gameId;                    // идентификатор игры
    private GameService gameService;        // объект бизнес-логики игры
    private Lock lock = new ReentrantLock();

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    // метод запуска сервера на определенном порту
    public void start(int port) {
        try {
            // запустили SocketServer на определенном порту
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");
            // ждем первого
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО КЛИЕНТА...");
            // connect() будет висеть, пока не подключится клиент
            firstPlayer = connect();
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО КЛИЕНТА...");
            // ждем второго
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждет подключения клиента, и возвращает объект ClientThread который представляет собой отдельный поток
    // в рамках которого происходит общение с клиентом со стороны сервера
    private ClientThread connect() {
        Socket client;
        try {
            // уводит приложение в ожидание (wait) пока не присоединиться какой-либо клиент
            // как только клиент подключен к серверу
            // объект-соединение возвращается как результат выполнения метода
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // создали сокет-клиенту отдельный поток
        ClientThread clientThread = new ClientThread(client);
        // запустили этот поток
        clientThread.start();
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН...");
        // отправили клиенту сообщение о том, что он подключен
        clientThread.sendMessage("ВЫ ПОДКЛЮЧЕНЫ К СЕРВЕРУ.");
        return clientThread;
    }

    // отдельный поток для клиента
    private class ClientThread extends Thread {
        private final PrintWriter toClient;         // что мы хотим отправить клиенту
        private final BufferedReader fromClient;    // что мы хотим получить от клиента
        private String playerNickname;              // имя игрока, который "сидит" в текущем потоке
        private String ip;                          // ip игрока, который "сидит" в текущем потоке

        // задача конструктора - получить потоки для чтения/записи
        public ClientThread(Socket client) {
            try {
                // autoFlush - чтобы сразу отправлял данные в поток, а не ждал пока не вызовут принудительно flush
                // оборачиваем байтовые потоки в символьные потоки
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        // это метод, который работает в отдельном потоке на протяжении всей программы
        @Override
        public void run() {
            // бесконечный цикл (работает, пока игра запущена и клиент подключен)
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    // получили сообщение от клиента
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                // если сообщение не пустое
                if (messageFromClient != null) {
                    // если сообщение начинается с префикса name:
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        // только один поток может завершит в конкретный момент времени игру
                        lock.lock();
                        resolveExit();
                        lock.unlock();
                    } else if (isMessageForMove(messageFromClient)) {
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)) {
                        resolveShot(messageFromClient);
                    } else if (isMessageForDamage(messageFromClient)) {
                        resolveDamage();
                    }
                }
                lock.lock();
                if (isReadyForStartGame()) {
                    // вызываем метод бизнес логики для начала игры
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(), firstPlayer.playerNickname, secondPlayer.playerNickname);
                    // устанавливаем флаг начала игры
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private void resolveExit() {
            String message = gameService.finishGame(gameId).toString();
            String[] mess = message.split("\n");
            StringBuilder s = new StringBuilder();
            for (String value : mess) {
                s.append(value).append("; ");
            }
            firstPlayer.sendMessage(s.toString());
            secondPlayer.sendMessage(s.toString());
            firstPlayer.sendMessage("exit");
            secondPlayer.sendMessage("exit");
            isGameInProcess = false;
            try {
                toClient.close();
                fromClient.close();
                serverSocket.close();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        private void resolveDamage() {
            if (meFirst()) {
                gameService.shot(gameId, firstPlayer.playerNickname, secondPlayer.playerNickname);
            } else {
                gameService.shot(gameId, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }

        private void resolveShot(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveMove(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveNickname(String messageFromClient) {
            if (meFirst()) {
                fixNickname(messageFromClient, firstPlayer, "ИМЯ ПЕРВОГО ИГРОКА: ", secondPlayer);
            } else {
                fixNickname(messageFromClient, secondPlayer, "ИМЯ ВТОРОГО ИГРОКА: ", firstPlayer);
            }
        }

        private boolean isReadyForStartGame() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }

        private void fixNickname(String nickname, ClientThread currentPlayer, String anotherMessagePrefix, ClientThread anotherPlayer) {
            currentPlayer.playerNickname = nickname.substring(6);
            System.out.println(anotherMessagePrefix + nickname);
            anotherPlayer.sendMessage(nickname);
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            return this == firstPlayer;
        }

        public String getIp() {
            return ip;
        }
    }
}
