package ru.inno.game.server;

/**
 * game
 * 05.05.2021
 *
 * @author Anastasiya Zhalnina
 */

public class CommandsParser {
    public static boolean isMessageForDamage(String messageFromClient) {
        return messageFromClient.equals("DAMAGE");
    }

    public static boolean isMessageForShot(String messageFromClient) {
        return messageFromClient.equals("shot");
    }

    public static boolean isMessageForExit(String messageFromClient) {
        return messageFromClient.equals("exit");
    }

    public static boolean isMessageForNickname(String messageFromClient) {
        return messageFromClient.startsWith("name: ");
    }

    public static boolean isMessageForMove(String messageFromClient) {
        return messageFromClient.equals("left") || messageFromClient.equals("right");
    }
}
