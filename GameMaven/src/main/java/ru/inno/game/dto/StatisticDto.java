package ru.inno.game.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

/**
 * Game
 * 26.03.2021
 *
 * @author Anastasiya Zhalnina
 * Информация об игре
 */

@Data
@AllArgsConstructor
public class StatisticDto {
    private Game game;

    @Override
    public String toString() {
        Player winner; //победитель
        //если у первого игрока попаданий больше
        if (game.getCountShotsPlayerFirst() > game.getCountShotsPlayerSecond()) {
            //победитель первый игрок
            winner = game.getPlayerFirst();
            //если у второго игрока попаданий больше
        } else if (game.getCountShotsPlayerFirst() < game.getCountShotsPlayerSecond()) {
            //победитель второй игрок
            winner = game.getPlayerSecond();
            //если равное количество попаданий у игроков, то победила дружба
        } else {
            return "Игра с ID = " + game.getId() + "\n" +
                    "Игрок 1: " + game.getPlayerFirst().getName() + ", попаданий - " + game.getCountShotsPlayerFirst() + ", всего очков - " + game.getPlayerFirst().getPoints() + "\n" +
                    "Игрок 2: " + game.getPlayerSecond().getName() + ", попаданий - " + game.getCountShotsPlayerSecond() + ", всего очков - " + game.getPlayerSecond().getPoints() + "\n" +
                    "Победила дружба!" + "\n" +
                    "Игра длилась: " + game.getSecondsGameTimeAmount() + " секунд";
        }
        return "Игра с ID = " + game.getId() + "\n" +
                "Игрок 1: " + game.getPlayerFirst().getName() + ", попаданий - " + game.getCountShotsPlayerFirst() + ", всего очков - " + game.getPlayerFirst().getPoints() + "\n" +
                "Игрок 2: " + game.getPlayerSecond().getName() + ", попаданий - " + game.getCountShotsPlayerSecond() + ", всего очков - " + game.getPlayerSecond().getPoints() + "\n" +
                "Победа: " + winner.getName() + "\n" +
                "Игра длилась: " + game.getSecondsGameTimeAmount() + " секунд";
    }
}
