package ru.inno.game.repository;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

/**
 * Game
 * 13.04.2021
 *
 * @author Anastasiya Zhalnina
 */

public class GameRepositoryJdbcImpl implements GameRepository {

    // запросы, которые мы посылаем в базу данных
    //language=SQL
    private static final String SQL_INSERT_GAME =
            "insert into game(datetime, playerfirst_id, playersecond_id, countshotsplayerfirst, countshotsplayersecond," +
                    "  secondsgametimeamount) " +
                    "values (?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_FIND_GAME_BY_ID =
                    "select g.id                     as game_id," +
                    "       g.dateTime               as game_datetime," +
                    "       p.id                     as playerfirst_id," +
                    "       p.ipAddress              as playerfirst_ip," +
                    "       p.name                   as playerfirst_name," +
                    "       p.points                 as playerfirst_points," +
                    "       p.countwins              as playerfirst_countwins," +
                    "       p.countLoses             as playerfirst_countloses," +
                    "       p2.id                    as playersecond_id," +
                    "       p2.ipAddress             as playersecond_ip," +
                    "       p2.name                  as playersecond_name," +
                    "       p2.points                as playersecond_points," +
                    "       p2.countwins             as playersecond_countwins," +
                    "       p2.countLoses            as playersecond_countloses," +
                    "       g.countShotsPlayerFirst  as playerfirst_countshots," +
                    "       g.countShotsPlayerSecond as playersecond_countshots," +
                    "       g.secondsGameTimeAmount  as game_secondsgame " +
                    "from game as g" +
                    "         join player as p on g.playerFirst_id = p.id" +
                    "         join player p2 on g.playerSecond_id = p2.id " +
                    "where g.id = ?";

    //language=SQl
    private static final String SQL_UPDATE_GAME_BY_ID =
                    "update game " +
                    "set datetime               = ?," +
                    "    playerfirst_id         = ?," +
                    "    playersecond_id        = ?," +
                    "    countshotsplayerfirst  = ?," +
                    "    countshotsplayersecond = ?," +
                    "    secondsgametimeamount  =?" +
                    "where id = ?";

    // анонимный класс, реализованный через lambda выражение, который позволяет преобразовать строку из ResultSet в объект Player
    static private final RowMapper<Player> playerFirstRowMapper = row -> Player.builder()
            .id(row.getLong("playerfirst_id"))
            .ipAddress(row.getString("playerfirst_ip"))
            .name(row.getString("playerfirst_name"))
            .points(row.getInt("playerfirst_points"))
            .countWins(row.getInt("playerfirst_countwins"))
            .countLoses(row.getInt("playerfirst_countloses"))
            .build();

    static private final RowMapper<Player> playerSecondRowMapper = row -> Player.builder()
            .id(row.getLong("playersecond_id"))
            .ipAddress(row.getString("playersecond_ip"))
            .name(row.getString("playersecond_name"))
            .points(row.getInt("playersecond_points"))
            .countWins(row.getInt("playersecond_countwins"))
            .countLoses(row.getInt("playersecond_countloses"))
            .build();

    // анонимный класс, реализованный через lambda выражение, который позволяет преобразовать строку из ResultSet в объект Game
    static private final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("game_id"))
            .dateTime(row.getTimestamp("game_datetime").toLocalDateTime())
            .playerFirst(playerFirstRowMapper.mapRow(row))
            .playerSecond(playerSecondRowMapper.mapRow(row))
            .countShotsPlayerFirst(row.getInt("playerfirst_countshots"))
            .countShotsPlayerSecond(row.getInt("playersecond_countshots"))
            .secondsGameTimeAmount(row.getLong("game_secondsgame"))
            .build();

    //зависимость
    private DataSource dataSource;

    public GameRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             //объект который умеет выполнять запросы
             // RETURN_GENERATED_KEYS означает, что запрос должен вернуть ключи, которые сгенерировала база данных
             // для текущего запроса
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setObject(1, game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getCountShotsPlayerFirst());
            statement.setInt(5, game.getCountShotsPlayerSecond());
            statement.setLong(6, game.getSecondsGameTimeAmount());

            // сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            // ключи, которые база сгенерировала сама для этого запроса
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                // проверяем, сгенерировала ли база что-либо?
                if (generatedId.next()) {
                    // получаем ключ, который сгенерировала база для текущего запроса
                    game.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID)) {
            statement.setLong(1, gameId);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return gameRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID)) {
            statement.setObject(1, game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getCountShotsPlayerFirst());
            statement.setInt(5, game.getCountShotsPlayerSecond());
            statement.setLong(6, game.getSecondsGameTimeAmount());
            statement.setLong(7, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
