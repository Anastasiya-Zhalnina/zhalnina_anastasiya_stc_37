package ru.inno.game.repository;

import ru.inno.game.models.Player;

/**
 * Game
 * 25.03.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface PlayersRepository {
    /**
     * Метод получения игрока по его имени
     *
     * @param nickname - имя игрока
     * @return найденный игрок
     */
    Player findByNickname(String nickname);

    /**
     * Метод сохранения игрока
     *
     * @param player - игрок
     */
    void save(Player player);

    /**
     * Метод обновления игрока
     *
     * @param player - игрок
     */
    void update(Player player);
}

