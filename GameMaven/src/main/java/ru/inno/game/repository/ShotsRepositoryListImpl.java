package ru.inno.game.repository;

import ru.inno.game.models.Shot;

import java.util.ArrayList;
import java.util.List;

/**
 * Game
 * 26.03.2021
 *
 * @author Anastasiya Zhalnina
 * Имплементация на основе листа
 */

public class ShotsRepositoryListImpl implements ShotsRepository {

    private List<Shot> shots;

    public ShotsRepositoryListImpl() {
        this.shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        this.shots.add(shot);
    }
}
