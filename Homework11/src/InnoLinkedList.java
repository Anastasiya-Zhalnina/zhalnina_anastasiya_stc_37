public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;
    private int count;


    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: реализовать
        Node previous = null;
        Node current = first;
        for(int i = 0; i < count; i++) {
            if (i == index) {
                if (previous != null) {
                    if (previous.next != null) {
                        previous.next.value = element;
                    }
                    if (current != null && current.next == null) {
                        last = previous;
                    }
                } else {
                    first.value = element;
                }
            }
            previous = current;
            current = current.next;
        }
    }

    @Override
    public void addToBegin(int element) {
        // TODO: реализовать
        Node newNode = new Node(element);
        Node current = first;
        first = newNode;
        first.next = current;
        if (count == 0){
           last = first;
        }
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
        Node previous = null;
        Node current = first;
        for(int i = 0; i < count; i++){
            if (i == index){
                if (previous != null){
                    if (current != null) {
                        previous.next = current.next;
                    }
                    if (current != null && current.next == null) {
                        last = previous;
                    }
                }else{
                    first = first.next;
                    if (first == null){
                        last = null;
                    }
                }
                count--;
            }
            previous = current;
            current = current.next;
        }
    }


    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        // TODO: реализовать
        Node previous = null;
        Node current = first;
        while (current != null){
            if (current.value == element){
                if (previous != null){
                    previous.next = current.next;
                    if (current.next == null){
                        last = previous;
                    }
                }else{
                    first = first.next;
                    if (first == null){
                        last = null;
                    }
                }
                count--;
            }
            previous = current;
            current = current.next;
        }
    }

    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        Node current = first;
        for (int i = 0; i < count; i++) {
            if (current.value == element) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current != null;
        }
    }
}

