public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;
    private int[] elements;
    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: реализовать
        int[] newElements = new int[elements.length];
        int i = 0;
            for(int j =  0; j < count; j++){
                if(j != index){
                    newElements[i] = elements[j];
                    i++;
                }else{
                   newElements[index] = element;
                   i++;
                }
            }
        this.elements = newElements;
    }

    @Override
    public void addToBegin(int element) {
        // TODO: реализовать
        int[] newElements = new int[elements.length];
        if (count == elements.length) {
            newElements = new int[elements.length + elements.length / 2];
        }
        newElements[0] = element;
        count++;
        for (int i = 1; i < count; i++) {
            newElements[i] = elements[i - 1];
        }
        this.elements = newElements;
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
        int newCount = 0;
        int [] newElements = new int[elements.length];
        int i = 0;
             for (int j = 0; j < count; j++) {
                 if (j != index) {
                    newElements[i] = elements[j];
                    i++;
                    newCount++;
                }
            }
        count = newCount;
        this.elements = newElements;
    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }
        elements[count++] = element;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int[] newElements = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        // TODO: реализовать
        int[] newElements = new int[elements.length];
        int newCount = 0;
        int i = 0;
            for(int j = 0; j < count; j++) {
                if (elements[j] != element) {
                    newElements[i] = elements[j];
                    i++;
                    newCount++;
                }
            }
        count = newCount;
        this.elements = newElements;
}

    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        for (int i = 0; i < count; i++){
            if(elements[i] == element){
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}

