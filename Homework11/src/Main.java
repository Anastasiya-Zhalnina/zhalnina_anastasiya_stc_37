public class Main {

    public static void main(String[] args) {
        // создаем список
        InnoList list = new InnoArrayList();
        for(int i = 0; i < 10; i++) {
            list.add(i);
        }

        // выводим коллекцию, получив элементы колллекции
        System.out.println("-----------InnoArrayList-----------");
        System.out.print("Исходный лист: ");
        print(list);
        System.out.println();

        // проверяем содержится ли элемент в коллекции
        System.out.println("Содержит ли лист заданный элемент? - " + list.contains(5));
        System.out.println("Содержит ли лист заданный элемент? - " + list.contains(10));

        //удаляем элемент коллекции
        list.remove(7);
        System.out.print("Лист после удаления заданных элементов: ");
        print(list);

        //удаляем элемент по индексу
        list.removeByIndex(3);
        System.out.println();
        System.out.print("Элемент под заданным индексом был удален: ");
        print(list);

        //добавляем элемент в начало коллекции
        list.addToBegin(100);
        System.out.println();
        System.out.print("Добавили заданный элемент в начало листа: ");
        print(list);

        //Заменяем элемент в определенном индексе
        list.insert(5, 13);
        System.out.println();
        System.out.print("Заменили элемент в заданном индексе: ");
        print(list);

        //итератор
        InnoIterator iterator = list.iterator();
        System.out.println();
        System.out.print("Проверка итератора: ");
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        System.out.println();

        // создаем список
        InnoList listTwo = new InnoLinkedList();
        for(int i = 10; i > 0; i--) {
            listTwo.add(i);
        }


        // выводим коллекцию, получив элементы колллекции
        System.out.println("-----------InnoLinkedList-----------");
        System.out.print("Исходный лист: ");
        print(listTwo);
        System.out.println();

        // проверяем содержится ли элемент в коллекции
        System.out.println("Содержит ли лист заданный элемент? - " + listTwo.contains(5));
        System.out.println("Содержит ли лист заданный элемент? - " + listTwo.contains(0));

        //удаляем элемент коллекции
        listTwo.remove(8);
        System.out.print("Лист после удаления заданных элементов: ");
        print(listTwo);

        //удаляем элемент по индексу
        listTwo.removeByIndex(3);
        System.out.println();
        System.out.print("Элемент под заданным индексом был удален: ");
        print(listTwo);

        //добавляем элемент в начало коллекции
        listTwo.addToBegin(300);
        System.out.println();
        System.out.print("Добавили заданный элемент в начало листа: ");
        print(listTwo);

        ////Заменяем элемент в определенном индексе
        listTwo.insert(2, 50);
        System.out.println();
        System.out.print("Заменили элемент в заданном индексе: ");
        print(listTwo);

        //итератор
        InnoIterator iteratorTwo = listTwo.iterator();
        System.out.println();
        System.out.print("Проверка итератора: ");
        while (iteratorTwo.hasNext()) {
            System.out.print(iteratorTwo.next() + " ");
        }

    }

    public static void print(InnoList list){
        for(int i = 0; i < list.size(); i++){
            System.out.print(list.get(i) + " ");
        }
    }
}
