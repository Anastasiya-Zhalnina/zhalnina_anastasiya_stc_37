/* Реализовать приложение, рассчитывающее определенный интеграл методом Симпсона */

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите нижнее значение a = ");
        double a = scanner.nextDouble();
        System.out.print("Введите верхнее значение b = ");
        double b = scanner.nextDouble();
        int[] steps = {10 , 100, 1_000, 10_000, 100_000, 1_000_000};
        System.out.printf("%7s| %20s| \n", "N", "Площадь фигуры");
        for (int i = 0; i < steps.length; i++){
            System.out.printf("%7d| %20.13f| \n", steps[i], calcIntegral(a, b, steps[i]));
        }
    }

    public static double f(double x){
        return  x*x;
    }

    public static double calcIntegral(double a, double b, int n){
        double h = (b - a) / (2 * n);
        double[] x = new double[2 * n + 1];
        for (int i = 0; i <= 2 * n; i++){
            x[i] = a + i * h;
        }
        double integral;
        double sumOne = 0;
        double sumTwo = 0;
        for(int i = 1; i <= n; i++){
            sumOne +=  f(x[2 * i - 1]);
        }

        for(int i = 1; i <= n-1; i++){
            sumTwo +=  f(x[2 * i]);
        }

        integral = h/3 *(f(x[0]) + 4 * sumOne + 2* sumTwo + f(x[2*n]));
        return integral;
    }
}
