/*Реализовать приложение, со следующим набором функций и процедур:
1. выводит сумму элементов массива
2. выполняет разворот массива (массив вводится с клавиатуры).
3. вычисляет среднее арифметическое элементов массива (массив вводится с клавиатуры).
4. меняет местами максимальный и минимальный элементы массива
5. выполняет сортировку массива методом пузырька.
6. выполняет преобразование массива в число. */

import java.util.Arrays;
import java.util.Scanner;

class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива: ");
        int n = scanner.nextInt();
        int[] array = new int [n];
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        sumOfElements(array);
        System.out.println("Развернутый массив: " + Arrays.toString(reverseOfArray(array)));
        System.out.println("Среднее арифметическое элементов массива равно: " + averageValue(array));
        System.out.println("Поменяли местами минимальный и максимальный элемент: " + Arrays.toString(minSwapMax(array)));
        System.out.println("Отсортированный массив: " + Arrays.toString(sortBubble(array)));
        System.out.println("Число из элементов массива: " + numberOfElements(array));

    }

    //выводит сумму элементов массива
    public static void sumOfElements(int[] array){
        int sumOfElements = 0;
        for (int i = 0 ; i < array.length; i++){
            sumOfElements += array[i];
        }
        System.out.println("Сумма элементов массива равна: " + sumOfElements);
    }

    //выполняет разворот массива
    public static int[] reverseOfArray(int[] array){
        int j = 0;
        for (int i = array.length - 1; i >= array.length/2; i--){
            int temp = array[j];
            array[j] = array[i];
            array[i] = temp;
            j++;
        }
        return array;
    }

    // вычисляет среднее арифметическое элементов массива
    public static double averageValue (int[] array){
        double sumOfElements = 0;
        for (int i = 0 ; i < array.length; i++){
            sumOfElements += array[i];
        }
        return sumOfElements/array.length;
    }

    //меняет местами максимальный и минимальный элементы массива
    public static int[] minSwapMax (int[] array){
        int min = array[0];
        int max = array[0];
        int positionMin = 0;
        int positionMax = 0;
        for(int i = 0; i < array.length; i++){
            if(array[i] < min){
                min = array[i];
                positionMin = i;
            }
            if(array[i] > max){
                max = array[i];
                positionMax = i;
            }
        }
        int temp = array[positionMax];
        array[positionMax] = array[positionMin];
        array[positionMin] = temp;
        return array;
    }

    //выполняет сортировку массива методом пузырька.
    public static int[] sortBubble(int[] array){
        for (int j = array.length - 1; j >= 1; j--){
            for (int i = 0; i < j; i++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

    //выполняет преобразование массива в число
    public static int numberOfElements(int[] array){
        int number = 0;
        String numberString = "";
        for(int i = 0; i < array.length; i++){
            numberString += array[i];
        }
        number = Integer.parseInt(numberString);
        return number;
    }
}
