/* Реализовать приложение, которое выводит сумму цифр пятизначного числа (значение числа
задается непосредственно в коде). В данном задании запрещено использование циклов и массивов. */

class Program {
    public static void main(String args[]) {
        int number = 12345;
        int oneNumber = number % 10;
        int twoNumber = number/10 % 10;
        int threeNumber = number/100 % 10;
        int fourNumber = number/1000 % 10;
        int fiveNumber = number/10000 % 10;
        int digitsSum = oneNumber + twoNumber + threeNumber + fourNumber + fiveNumber;

        System.out.println(digitsSum);
    }
}
