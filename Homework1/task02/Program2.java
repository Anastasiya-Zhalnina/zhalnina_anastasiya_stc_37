class Program2 {
    public static void main(String[] args) {
        int number = 99999;
        binarySystem(number);
    }
    private static void binarySystem(int number){
        if (number < 2){
            System.out.print(number);
            return;
        }
        int binaryNumber = number % 2;
        binarySystem(number/2);
        System.out.print(binaryNumber);
    }
}
