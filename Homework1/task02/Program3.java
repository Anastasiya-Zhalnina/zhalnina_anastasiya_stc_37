package part1.lesson1.task2;

public class Program3 {
    public static void main(String args[]) {
        int number = 99999;
        String binaryNumber = "";
        while (number != 0){
            binaryNumber = number % 2 + binaryNumber;
            number = number/2;
        }
        System.out.println(binaryNumber);
    }
}
