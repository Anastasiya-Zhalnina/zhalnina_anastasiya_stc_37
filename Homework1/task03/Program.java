/*Реализовать приложение, которое для заданной последовательности чисел считает
произведение тех чисел, сумма цифр которых - простое число. Последнее число
последовательности - 0.*/

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int sum = 0;
        long multiplication = 1;
        while (number != 0) {
            int startNumber = number;
            //находим сумму чисел
            while (number != 0) {
                sum = sum + number % 10;
                number = number / 10;
            }
            // проверяем состовное или простое число
            boolean compositeNumber = false;
            for (int i = 2; i < sum; i++){
                if (sum % i == 0){
                    compositeNumber = true;
                    break;
                }
            }
            // ищем произведение чисел сумма цифр которых - простое число
            if (!compositeNumber){
                multiplication = multiplication * startNumber;
            }
            sum = 0;
            number = scanner.nextInt();
        }
        System.out.println(multiplication);
    }
}
