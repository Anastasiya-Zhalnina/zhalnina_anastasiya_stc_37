/* Реализовать Фибоначчи с однократным вызовом рекурсии.
Без f(n-1) + f(n-2) */

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите индекс числа Фибоначчи: ");
        int n = scanner.nextInt();
        System.out.println("Число под индексом " + n + " в системе Фиббоначи равно: " + fibonacci(n));

    }
    public static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1 || n == 2 || n == - 1){
            return 1;
        }
        if(n > 2) {
            return calcFibonacciPositive(n, 2, 1, 1);
        }
            return calcFibonacciNegative(n, -1, 1, 0);
    }

    public  static int calcFibonacciPositive (int n, int previous, int fibPrevious, int fibPreviousMinusOne){
        int current = previous + 1;
        int fibCurrent = fibPrevious + fibPreviousMinusOne;
        if (n == current) {
            return fibCurrent;
        }
        return calcFibonacciPositive(n, current, fibCurrent, fibPrevious);
    }

    public static int calcFibonacciNegative (int n, int previous, int fibPrevious, int fibPreviousPlusOne){
        int current = previous - 1;
        int fibCurrent = fibPreviousPlusOne - fibPrevious;
        if (n == current){
            return  fibCurrent;
        }
        return calcFibonacciNegative(n, current, fibCurrent, fibPrevious);
    }
}
