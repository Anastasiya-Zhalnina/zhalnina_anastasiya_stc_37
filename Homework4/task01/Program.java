/* Реализовать рекурсивную функцию, проверяющую, является ли число степенью двойки. */

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        double number = scanner.nextDouble();
        boolean result = isPowerOfTwo(number);
        if(result){
            System.out.println("Число " + number + " является степенью двойки.");
        }else{
            System.out.println("Число " + number + " не является степенью двойки.");
        }
    }

    public static boolean isPowerOfTwo(double number) {
        if (number ==  1){
            return true;
        } else if (number > 1 && number < 2) {
            return false;
        }else{
            return isPowerOfTwo(number/2);
        }
    }
}
