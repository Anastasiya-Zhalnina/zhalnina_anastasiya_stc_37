/* Реализовать метод бинарного поиска с помощью рекурсии. */

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = {-1, 7, 22, 28, 31, 33, 49, 55, 87, 90};
        System.out.print("Ввеите число: ");
        int number = scanner.nextInt();
        boolean result = isBinarySearch(array, number);
        if (result){
            System.out.println("Число " + number + " содержится в заданном массиве.");
        }else{
            System.out.println("Число " + number + " не содержится в заданном массиве.");
        }

    }
    public static boolean isBinarySearch(int[] array, int number){
        return binarySearch(array, number, 0, array.length -1);
    }
    public  static boolean binarySearch(int[] array, int number, int left, int right){
        boolean isExist = false;
        int middle = left + (right  -  left) / 2;
        if (left <= right) {
            if (array[middle] < number) {
                isExist = binarySearch(array, number, middle + 1, right);
            } else if (array[middle] > number) {
                isExist = binarySearch(array, number, left, middle - 1);
            } else if (array[middle] == number) {
                isExist = true;
            }
        }
        return isExist;
    }
}
