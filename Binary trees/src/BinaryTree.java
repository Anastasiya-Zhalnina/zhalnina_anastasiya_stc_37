/**
 * Binary trees
 * 07.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface BinaryTree<T extends Comparable<T>> {
    /**
     * Поиск элемента по значению
     *
     * @param value - искомое значение
     * @return - найденный узел
     */
    BinaryTreeImpl.Node<T> findNodeByValue(T value);

    /**
     * Вставка элемента
     *
     * @param value - втавляемый элемент
     */
    void insert(T value);

    /**
     * Обход дерева в глубину
     */
    void printDfs();

    /**
     * Обход дерева в ширину
     */
    void printBfs();

    /**
     * Обход дерева в глубину по стеку
     */
    void printDfsByStack();

    /**
     * Печать дерева в консоль
     */
    void printTree();

    /**
     * Удаление узла с заданным значением
     * @param value - значение удаляемого узла
     * @return результат удаления элемента
     */
    boolean deleteNode(T value);
}
