public class Main {

    public static void main(String[] args) {
        BinaryTree<Integer> tree = new BinaryTreeImpl<>();
        tree.insert(7);
        tree.insert(3);
        tree.insert(9);
        tree.insert(2);
        tree.insert(5);
        tree.insert(8);
        tree.insert(10);
        tree.insert(1);
        tree.insert(4);
        tree.insert(6);
        tree.insert(11);

        tree.printTree();

        System.out.println("Искомый узел: " + tree.findNodeByValue(5));

        System.out.print("Обход дерева в глубину: ");
        tree.printDfs();
        System.out.println();

        System.out.print("Обход дерева в ширину: ");
        tree.printBfs();
        System.out.println();

        System.out.print("Обход дерева в глубину по стеку: ");
        tree.printDfsByStack();
        System.out.println();

        tree.deleteNode(9);
        tree.printTree();
    }
}
