import java.util.*;

/**
 * Binary trees
 * 07.04.2021
 *
 * @author Anastasiya Zhalnina
 */
public class BinaryTreeImpl<T extends Comparable<T>> implements BinaryTree<T> {
    static class Node<E> {
        E value; // значение узла
        Node<E> left; //левый узел потомок
        Node<E> right; //правый узел потомок

        public Node(E value) {
            this.value = value;
        }

        public E getValue() {
            return value;
        }

        public void setValue(E value) {
            this.value = value;
        }

        public Node<E> getLeft() {
            return left;
        }

        public void setLeft(Node<E> left) {
            this.left = left;
        }

        public Node<E> getRight() {
            return right;
        }

        public void setRight(Node<E> right) {
            this.right = right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(value, node.value) && Objects.equals(left, node.left) && Objects.equals(right, node.right);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, left, right);
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", Node.class.getSimpleName() + "[", "]")
                    .add("value=" + value)
                    .add("left=" + left)
                    .add("right=" + right)
                    .toString();
        }
    }

    private Node<T> root; // корневой узел

    public BinaryTreeImpl() {
        this.root = null;
    }

    //поиск элемента по значению
    @Override
    public Node<T> findNodeByValue(T value) {
        //начинаем поиск с корнегого узла
        Node<T> currentNode = root;
        //пока не найдем заданный элемент
        while (!currentNode.getValue().equals(value)) {
            //если искомый элемент меньше мы переходим к левому потомку
            if (value.compareTo(currentNode.getValue()) < 0) {
                currentNode = currentNode.getLeft();
                //если искомый элемент больше мы переходим к правому потомку
            } else {
                currentNode = currentNode.getRight();
            }
            //если потомка нет возвращаем null
            if (currentNode == null) {
                return null;
            }
        }
        return currentNode; //возвращаем найденный элемент
    }

    //вставка элемента
    @Override
    public void insert(T value) {
        this.root = insert(root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        // если корневого элемента нет, то новый элемент и есть корневой
        if (root == null) {
            root = new Node<>(value);
            //Если новый элемент меньше, то переходим к левому наследнику, если его нет, новый элемент и занимает место левого наследника
        } else if (value.compareTo(root.value) < 0) {
            root.left = insert(root.left, value);
            //Если новый элемент больше, то переходим к правому наследнику, если его нет, новый элемент и занимает место правого наследника
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }

    //обход дерева в глубину
    @Override
    public void printDfs() {
        dfs(root);
    }

    private void dfs(Node<T> root) {
        if (root != null) {
            dfs(root.left);
            System.out.print(root.value + " ");
            dfs(root.right);
        }
    }

    //обход дерева в ширину
    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        // кладем в очередь корень
        queue.add(root);

        Node<T> current;
        // пока очередь не пустая
        while (!queue.isEmpty()) {
            // забираем узел из конца очереди
            current = queue.poll();
            System.out.print(current.value + " ");
            // если у текущего узла есть левый потомок
            if (current.left != null) {
                // положим этого потомка в начало очереди
                queue.add(current.left);
            }
            // если у текущего узла есть правый потомок
            if (current.right != null) {
                // положим этого потомка в начало очереди
                queue.add(current.right);
            }
        }
    }

    //обход дерева в глубину по стеку
    @Override
    public void printDfsByStack() {
        Stack<Node<T>> stack = new Stack<>();
        // кладем в начало стека
        stack.push(root);

        Node<T> current;
        // пока очередь не пустая
        while (!stack.isEmpty()) {
            // забираем узел из начала стека
            current = stack.pop();
            // если у текущего узла есть левый потомок
            if (current.left != null) {
                // положим этого потомка в начало стека
                stack.push(current.left);
            }
            System.out.print(current.value + " ");
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    //печать в консоль дерева
    public void printTree() { // метод для вывода дерева в консоль
        Stack<Node<T>> stack = new Stack<>(); // общий стек для значений дерева
        stack.push(root);
        int gaps = 32; // начальное значение расстояния между элементами
        boolean isRowEmpty = false;
        String separator = "-------------------------------------------------------------------------------------------";
        System.out.println();
        System.out.println("                                Бинарное дерево");
        System.out.println(separator);// черта для указания начала нового дерева
        while (!isRowEmpty) {
            Stack<Node<T>> localStack = new Stack<>(); // локальный стек для задания потомков элемента
            isRowEmpty = true;

            for (int j = 0; j < gaps; j++)
                System.out.print(' ');
            while (!stack.isEmpty()) { // покуда в общем стеке есть элементы
                Node<T> temp = stack.pop(); // берем следующий, при этом удаляя его из стека
                if (temp != null) {
                    System.out.print(temp.getValue()); // выводим его значение в консоли
                    localStack.push(temp.getLeft()); // соохраняем в локальный стек, наследники текущего элемента
                    localStack.push(temp.getRight());
                    if (temp.getLeft() != null ||
                            temp.getRight() != null)
                        isRowEmpty = false;
                } else {
                    System.out.print("__");// - если элемент пустой
                    localStack.push(null);
                    localStack.push(null);
                }
                for (int j = 0; j < gaps * 2 - 2; j++)
                    System.out.print(' ');
            }
            System.out.println();
            gaps /= 2;// при переходе на следующий уровень расстояние между элементами каждый раз уменьшается
            while (!localStack.isEmpty())
                stack.push(localStack.pop()); // перемещаем все элементы из локального стека в глобальный
        }
        System.out.println(separator);// подводим черту
    }

    @Override
    public boolean deleteNode(T value) {
        //ищем узел, который требуется удалить
        //начинаем поиск с корнегого узла
        Node<T> currentNode = root;
        Node<T> parentNode = root;
        boolean isLeftChild = true;
        //пока не найдем заданный элемент
        while (!currentNode.getValue().equals(value)) {
            parentNode = currentNode;
            //если искомый элемент меньше мы переходим к левому потомку
            if (value.compareTo(currentNode.getValue()) < 0) {
                isLeftChild = true;
                currentNode = currentNode.getLeft();
                //если искомый элемент больше мы переходим к правому потомку
            } else {
                isLeftChild = false;
                currentNode = currentNode.getRight();
            }
            if (currentNode == null) {
                return false; //узел не найден
            }
        }
        //Если удаляемый узел является листовым (не имеет потомков)
        if (currentNode.getLeft() == null && currentNode.getRight() == null) {
            //Если удаляемый элемент корень, то очищаем дерево
            if (currentNode.equals(root)) {
                root = null;
                //Если это потомок
            } else if (isLeftChild) {
                //отсоединяем от родителя левого потомка
                parentNode.setLeft(null);
            } else {
                //отсоединяем от родителя правого потомка
                parentNode.setRight(null);
            }
            //Если удаляемый узел имеет одного потомка
            //Узел заменяется левым поддеревом, если правого потомка нет
        } else if (currentNode.getRight() == null) {
            if (currentNode.equals(root)) {
                root = currentNode.getLeft();
            } else if (isLeftChild) {
                parentNode.setLeft(currentNode.getLeft());
            } else {
                parentNode.setRight(currentNode.getLeft());
            }
            //Узел заменяется правым поддеревом, если левого потомка нет
        } else if (currentNode.getLeft() == null) {
            if (currentNode.equals(root)) {
                root = currentNode.getRight();
            } else if (isLeftChild) {
                parentNode.setLeft(currentNode.getRight());
            } else {
                parentNode.setRight(currentNode.getRight());
            }
            //Если узел имеет двух потомков
        } else {
            Node<T> heir = receiveHeir(currentNode); // ищем наследника для удаляемого узла
            heir.setLeft(currentNode.getLeft());
            if (currentNode.equals(root)) {
                root = heir;
            } else if (isLeftChild) {
                parentNode.setLeft(heir);

            } else {
                parentNode.setRight(heir);
            }
        }
        return true; //элемент успешно удален
    }

    //метод поиска наследника
    private Node<T> receiveHeir(Node<T> node) {
        Node<T> parentNode = node;
        Node<T> heirNode = node;
        Node<T> currentNode = node.getRight(); // Переход к правому потомку
        while (currentNode != null){ // Пока остаются левые потомки
            parentNode = heirNode;// потомка задаём как текущий узел
            heirNode = currentNode;
            currentNode = currentNode.getLeft(); // переход к левому потомку
        }
        // Если преемник не является правым потомком
        if (!(heirNode.equals(node.getRight()))){
            // создать связи между узлами
            parentNode.setLeft(heirNode.getRight());
            heirNode.setRight(node.getRight());
        }
        return heirNode;// возвращаем приемника
    }
}
