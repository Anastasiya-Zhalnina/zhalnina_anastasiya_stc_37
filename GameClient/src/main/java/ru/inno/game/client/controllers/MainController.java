package ru.inno.game.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import ru.inno.game.client.socket.SocketClient;
import ru.inno.game.client.utils.GameUtils;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * GameClient
 * 30.04.2021
 *
 * @author Anastasiya Zhalnina
 * Контроллер для управления компонентами
 */

public class MainController implements Initializable {

    private GameUtils gameUtils;
    private SocketClient socketClient;

    //связываем с соответствующим компонентом в файле
    @FXML
    private ImageView player;

    @FXML
    private ImageView enemy;

    @FXML
    private Button buttonGo;

    @FXML
    private Button buttonConnect;

    @FXML
    private TextField textPlayerName;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label hpPlayer;

    @FXML
    private Label hpEnemy;

    @FXML
    private Label finishGame;

    @FXML
    private Label exit;

    @FXML
    private ImageView fire;

    //обработчик события при нажатии кнопки
    private EventHandler<KeyEvent> keyEventEventHandler = event -> {
        if (event.getCode() == KeyCode.RIGHT) {
            gameUtils.goRight(player);
            socketClient.sendMessage("right");
        } else if (event.getCode() == KeyCode.LEFT) {
            gameUtils.goLeft(player);
            socketClient.sendMessage("left");
        } else if (event.getCode() == KeyCode.SPACE) {
            Circle bullet = gameUtils.createBulletFor(player, false);
            socketClient.sendMessage("shot");
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gameUtils = new GameUtils();
        buttonConnect.setOnAction(event -> {
            socketClient = new SocketClient(this, "localhost", 7777);
            new Thread(socketClient).start();
            buttonConnect.setDisable(true);
            buttonGo.setDisable(false);
            textPlayerName.setDisable(false);
            gameUtils.setPane(pane);
            gameUtils.setClient(socketClient);
        });
        buttonGo.setOnAction(event -> {
            socketClient.sendMessage("name: " + textPlayerName.getText());
            buttonGo.setDisable(true);
            textPlayerName.setDisable(true);
            buttonGo.getScene().getRoot().requestFocus();
        });

        gameUtils.setController(this);
        finishGame.setVisible(false);
    }

    public EventHandler<KeyEvent> getKeyEventEventHandler() {
        return keyEventEventHandler;
    }

    public GameUtils getGameUtils() {
        return gameUtils;
    }

    public ImageView getEnemy() {
        return enemy;
    }

    public ImageView getPlayer() {
        return player;
    }

    public Label getHpPlayer() {
        return hpPlayer;
    }

    public Label getHpEnemy() {
        return hpEnemy;
    }

    public Label getFinishGame() {
        return finishGame;
    }

    public Label getExit() {
        return exit;
    }

    public ImageView getFire() {
        return fire;
    }
}
