package ru.inno.game.client.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.inno.game.client.controllers.MainController;

/**
 * GameClient
 * 30.04.2021
 *
 * @author Anastasiya Zhalnina
 */

public class Main extends Application {
    public static void main(String[] args) {
        //launch() вызовет метод start()
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //загружаем файл fxml, сказав где находится файл
        String fxmlFileName = "/fxml/Main.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFileName));

        primaryStage.setTitle("Game Client");
        //создаем сцену, положив в неё наш файл
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        //чтоб размер окна нельзя было менять
        primaryStage.setResizable(false);
        //получим контроллер
        MainController controller = loader.getController();
        //взяли обработчик нажатий из контроллера и добавили его в сцену
        scene.setOnKeyPressed(controller.getKeyEventEventHandler());
        //показать сцену
        primaryStage.show();
    }
}
