package ru.inno.game.client.socket;

import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.utils.GameUtils;
import java.io.*;
import java.net.Socket;

/**
 * Sockets IO - Client
 * 23.04.2021
 *
 * @author Anastasiya Zhalnina
 * Поток для получения сообщения с сервера
 */

public class SocketClient extends Thread {
    private Socket socket;              // канал подключения
    private PrintWriter toServer;       // стрим для отправления сообщений серверу
    private BufferedReader fromServer;  // стрим для получения сообщений от сервера
    private MainController controller;
    private GameUtils gameUtils;
    private boolean isGameInProcess = true;

    public SocketClient(MainController controller, String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждем сообщений от сервера
    @Override
    public void run() {
        while (isGameInProcess) {
            try {
                // прочитали сообщение с сервера
                String messageFromServer = fromServer.readLine();
                if (messageFromServer != null && !messageFromServer.equals("exit")) {
                    if (messageFromServer.startsWith("Игра")) {
                        StringBuilder s = new StringBuilder();
                        String[] message = messageFromServer.split("; ");
                        for (String element : message) {
                            s.append(element).append("\n");
                        }
                        controller.getExit().setVisible(true);
                        controller.getFinishGame().setVisible(true);
                        Platform.runLater(() -> controller.getExit().setText("КОНЕЦ ИГРЫ!"));
                        Platform.runLater(() -> controller.getFinishGame().setText(s.toString()));
                        controller.getHpPlayer().setVisible(false);
                        controller.getHpEnemy().setVisible(false);
                    }
                    switch (messageFromServer) {
                        case "left":
                            //двигаем врага налево
                            gameUtils.goLeft(controller.getEnemy());
                            break;
                        case "right":
                            //двигаем врага направо
                            gameUtils.goRight(controller.getEnemy());
                            break;
                        case "shot":
                            Media sound = new Media(new File("src/main/resources/mp3/стрельба.mp3").toURI().toString());
                            MediaPlayer media = new MediaPlayer(sound);
                            media.play();
                            Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                            break;
                    }
                    System.out.println(messageFromServer);
                } else {
                    isGameInProcess = false;
                    toServer.close();
                    fromServer.close();
                    socket.close();
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }
}





