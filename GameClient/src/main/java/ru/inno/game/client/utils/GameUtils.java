package ru.inno.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.socket.SocketClient;

import java.io.File;

/**
 * GameClient
 * 30.04.2021
 *
 * @author Anastasiya Zhalnina
 */

public class GameUtils {
    private static final int PLAYER_STEP = 10;
    private static final int DAMAGE = 5;

    private AnchorPane pane;
    private MainController controller;
    private SocketClient client;
    private boolean isGameInProcess = true;

    //Движение влево
    public void goLeft(ImageView player) {
        if (player.getX() >= -40) {
            player.setX(player.getX() - PLAYER_STEP);
        }
    }

    //Движение вправо
    public void goRight(ImageView player) {
        if (player.getX() <= 640) {
            player.setX(player.getX() + PLAYER_STEP);
        }
    }

    //создание пули
    public Circle createBulletFor(ImageView player, boolean isEnemy) {
        Circle bullet = new Circle();
        bullet.setRadius(8);
        pane.getChildren().add(bullet);
        bullet.setCenterX(player.getX() + player.getLayoutX() + 45);
        bullet.setCenterY(player.getLayoutY() + 60);
        bullet.setFill(Color.ORANGE);

        int value;

        //выстрел вниз/вверх
        if (isEnemy) {
            value = 3;
        } else {
            value = -3;
        }

        final ImageView target;
        final Label targetHp;

        //определяем кто куда стреляет
        if (!isEnemy) {
            target = controller.getEnemy();
            targetHp = controller.getHpEnemy();
        } else {
            target = controller.getPlayer();
            targetHp = controller.getHpPlayer();
        }

        //анимация для того, чтобы пуля летела во врага
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bullet.setCenterY(bullet.getCenterY() + value);
            // если пуля еще видна и произошло пересечение
            if (bullet.isVisible() && isIntersects(bullet, target)) {
                if (Integer.parseInt(targetHp.getText()) > 0) {
                    //уменьшаем здоровье
                    createDamage(targetHp);
                } else {
                    if (isEnemy) {
                        controller.getFire().setY(target.getY() - 40);
                    } else {
                        controller.getFire().setY(target.getY());
                    }
                    controller.getFire().setX(target.getX());
                    controller.getFire().setLayoutX(target.getLayoutX());
                    controller.getFire().setLayoutY(target.getLayoutY());
                    target.setVisible(false);
                    Media sound = new Media(new File("src/main/resources/mp3/взрыв.mp3").toURI().toString());
                    MediaPlayer media = new MediaPlayer(sound);
                    media.play();
                    controller.getFire().setVisible(true);
                    client.sendMessage("exit");
                    isGameInProcess = false;
                }
                //скрыть пулю
                bullet.setVisible(false);
                if (!isEnemy) {
                    client.sendMessage("DAMAGE");
                }
            }
        }));
        timeline.setCycleCount(500);
        timeline.play();
        if (!isGameInProcess) {
            pane.getChildren().remove(bullet);
        }
        return bullet;
    }

    private boolean isIntersects(Circle bullet, ImageView player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    public void createDamage(Label hpLabel) {
        int hpPlayer = Integer.parseInt(hpLabel.getText());
        if (hpPlayer > 0) {
            hpLabel.setText(String.valueOf(hpPlayer - DAMAGE));
        }
    }

    public void setClient(SocketClient client) {
        this.client = client;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }
}